const
    Discord = require('discord.js'),
    polarity = require('polarity');

const inspect = object => require('util').inspect(object, undefined, null).replace(/ {2}/g, ' ');

let Client = new Discord.Client();

module.exports = {
    /**
     * @method discord
     * @description Discord integration
     * @param Window
     * @param help
     * @param h - Short for help
     * @param login - Login with bot token
     * @param l - Short for login
     * @param guildId - Guild ID
     * @param g - Short for guildId
     * @param userId - User ID
     * @param u - Short for userId
     */
    default: async ({ Window }, [], { help, h, login, l, guildId, g, userId, u }) => {
        if(help || h){
            return `Discord Integration v${require('./package').version}`
                + `\nUsage : discord [-h / --help]`
                + `\n        discord [-l / --login] <token>`
                + `\n        discord [-g / --guild-id <guildId>]`
                + `\n        discord [-u / --user-id <userId>]`;
        }
        else if(login || l) return await new Promise((resolve, reject) => {
            if(Client.destroy){
                Client.destroy();
                Client = new Discord.Client();
            }
            Client.once('ready', () => resolve(Client.user.id));
            Client.login(login || l).catch(reject);
        });
        else if(guildId || g){
            const w1 = new Window(25, 25, 'Seeking organization', undefined, true);
            w1.show();
            const guild = Client.guilds.cache.get(guildId || g);
            w1.setTitleLoading(false);
            w1.setTitleText(guild ? 'Organization identified' : 'Organization not found');
            w1.setSubtitleText(guild ? guild.name : guildId || g);
            if(guild){
                const p1 = new Window.Picture(`https://cdn.discordapp.com/icons/${guild.id}/${guild.icon}.jpg`);
                w1.addElement(p1);
            }
            else w1.setTitleWarning(true);
        }
        else if(userId || u){
            const w1 = new Window(25, 25, 'Seeking subject', undefined, true);
            w1.show();
            const user = await Client.users.fetch(userId || u);
            w1.setTitleLoading(false);
            w1.setTitleText(user ? 'Subject identified' : 'Subject not found');
            w1.setSubtitleText('ID : ' + (userId || u));
            if(user){
                w1.setSubtitleMetaText(`XXX-XX-${user.discriminator}`);
                const p1 = new Window.Picture(user.avatar ? `https://cdn.discordapp.com/avatars/${user.id}/${user.avatar}.png` : Window.Picture.NO_PHOTO_AVAILABLE);
                w1.addElement(p1);
                const tbl1 = new Window.Table(undefined, [
                    [ { text: 'Name' }, { text: user.username } ],
                    [ { text: 'Creation date' }, { text: Window.dateToString(user.createdAt) } ],
                    [ { text: 'Status' }, { text: user.presence.status } ]
                ]);
                w1.addElement(tbl1);
                if(user.bot){
                    if(user.id !== Client.user.id){
                        const st1 = new Window.Subtitle('Alert');
                        w1.addElement(st1);
                        const t2 = new Window.Text('Competing system', undefined, undefined, 'red');
                        w1.addElement(t2);
                    }
                    return;
                }
                w1.setSubtitleText('Tracking');
                w1.setSubtitleLoading(true);
                const userMessages = [];
                const guilds = Client.guilds.cache.array();
                for(let i = 0; i < guilds.length; i++){
                    const channels = guilds[i].channels.cache.array();
                    for(let j = 0; j < channels.length; j++){
                        const channel = channels[j];
                        if(channel.type === 'text'){
                            try {
                                await channel.messages.fetch();
                                const messages = channel.messages.cache.array();
                                userMessages.push(...messages.filter(message => message.author.id === user.id));
                            }
                            catch(_){}
                        }
                    }
                }
                if(!userMessages.length){
                    w1.setSubtitleText('Track lost');
                    w1.setSubtitleLoading(false);
                    w1.setSubtitleError(true);
                    return;
                }
                const sortedUserMessages = userMessages.sort((a, b) => b.createdTimestamp - a.createdTimestamp);
                const lastUserMessage = sortedUserMessages[0];
                const lastUserMessageGuild = lastUserMessage.guild;
                const lastUserMessageMember = lastUserMessage.member;
                const lastUserGuildMessages = sortedUserMessages.filter(message => message.guild.id === lastUserMessageGuild.id);
                const lastKnownLocationDateString = Window.dateToString(lastUserMessage.createdAt);
                tbl1.addLine([
                    { text: 'Last known location' },
                    { text: `${lastUserMessageGuild.name}\n${Window.dateToString(new Date()) === lastKnownLocationDateString ? 'Now' : lastKnownLocationDateString}` }
                ]);
                const st1 = new Window.Subtitle('Classification');
                w1.addElement(st1);
                let
                    classification = 'Irrelevant',
                    color;
                if(lastUserMessageMember.permissions.any(['MUTE_MEMBERS', 'DEAFEN_MEMBERS']))
                    classification = 'Secondary asset';
                if(lastUserMessageMember.permissions.any(['KICK_MEMBERS', 'BAN_MEMBERS', 'MANAGE_MESSAGES', 'MOVE_MEMBERS', 'MANAGE_NICKNAMES'])){
                    classification = 'Asset';
                    color = 'yellow';
                }
                if(lastUserMessageMember.permissions.any(['MANAGE_CHANNELS', 'MANAGE_GUILD', 'VIEW_AUDIT_LOG', 'MANAGE_ROLES'])){
                    classification = 'Primary Asset';
                    color = 'yellow';
                }
                if(lastUserMessageMember.permissions.has('ADMINISTRATOR')){
                    classification = 'Admin';
                    color = 'yellow';
                }
                const t1 = new Window.Text(classification, false, color);
                w1.addElement(t1);
                w1.setSubtitleText('Subject acquired');
                w1.setSubtitleLoading(false);
                const w2 = new Window(700, 25, 'Context analysis', undefined, true);
                w2.show();
                const sc1 = new Window.ScrollingData(inspect(lastUserMessageMember));
                w2.addElement(sc1);
                const tbl2 = new Window.Table(undefined, [
                    [ { text: 'Join date' }, { text: Window.dateToString(lastUserMessageMember.joinedAt) } ],
                    [ { text: 'Messages' }, { text: lastUserGuildMessages.length.toString() } ]
                ]);
                if(lastUserMessageMember.nickname)
                    tbl2.addLine([ { text: 'Alias' }, { text: lastUserMessageMember.nickname } ]);
                w2.addElement(tbl2);
                const st2 = new Window.Subtitle('Natural language processing', true);
                w2.addElement(st2);
                const tbl3 = new Window.Table(undefined, [
                    [ { text: 'Positivity' }, { text: '0%', color: 'green' } ],
                    [ { text: 'Negativity' }, { text: '0%', color: 'red'   } ]
                ]);
                w2.addElement(tbl3);
                let
                    totalPositivity = 0,
                    totalNegativity = 0;
                for(let i = 0; i < lastUserGuildMessages.length; i++){
                    const { positivity, negativity } = polarity(lastUserGuildMessages[i].content.split(' '));
                    totalPositivity += positivity;
                    totalNegativity += Math.abs(negativity);
                    const total = totalPositivity + totalNegativity;
                    tbl3.setData([
                        [ { text: 'Positivity' }, { text: total > 0 ? `${(totalPositivity / total * 100).toFixed(2)}%` : '-', color: total > 0 ? 'green' : 'white' } ],
                        [ { text: 'Negativity' }, { text: total > 0 ? `${(totalNegativity / total * 100).toFixed(2)}%` : '-', color: total > 0 ? 'red'   : 'white' } ]
                    ]);
                    w2.setProgress(i / lastUserGuildMessages.length * 100);
                    await new Promise(resolve => setTimeout(resolve, 25));
                }
                w2.unsetProgress();
                w2.removeElement(sc1);
                st2.setLoading(false);
                w2.setTitleLoading(false);
                const st3 = new Window.Subtitle('Conclusion');
                w2.addElement(st3);
                const t2 = new Window.Text(totalPositivity > totalNegativity ? 'Non-threat' : 'Potential threat');
                w2.addElement(t2);
                const st4 = new Window.Subtitle('Recommendation');
                w2.addElement(st4);
                const t3 = new Window.Text(totalPositivity > totalNegativity ? 'Disregard' : 'Monitor');
                w2.addElement(t3);
            }
            else w1.setTitleWarning(true);
        }
    },
    getClient: () => Client
}